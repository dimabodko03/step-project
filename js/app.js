// ---------------------------Service------------------------------------
const serviceButtons = document.querySelectorAll('.service-tabs-title');
const tabsContent = document.querySelectorAll('.tabs-content');

serviceButtons.forEach((button, i) => {
    button.addEventListener('click', (event) => {
        tabsContent.forEach(content => {
            content.style.display = 'none';
        });

        tabsContent[i].style.display = 'flex';

        serviceButtons.forEach(t => t.classList.remove('active-now'));
        button.classList.add('active-now');

        serviceButtons.forEach((btn) => {
            btn.classList.remove('service-background');
        });

        event.target.classList.add('service-background');
    });
});

// ---------------------------Work------------------------------------

// Gallery

const photos = [
    {
        category: "Graphic Design",
        src: '../images/work/Layer 24.png'
    },
    {
        category: "Graphic Design",
        src: '../images/work/Layer 25.png'
    },
    {
        category: "Graphic Design",
        src: '../images/work/Layer 26.png'
    },
    {
        category: "Graphic Design",
        src: '../images/work/Layer 27.png'
    },
    {
        category: "Graphic Design",
        src: '../images/work/Layer 28.png'
    },
    {
        category: "Graphic Design",
        src: '../images/work/Layer 29.png'
    },
    {
        category: "Web Design",
        src: '../images/work/Layer 30.png'
    },
    {
        category: "Web Design",
        src: '../images/work/Layer 31.png'
    },
    {
        category: "Web Design",
        src: '../images/work/Layer 32.png'
    },
    {
        category: "Web Design",
        src: '../images/work/Layer 33.png'
    },
    {
        category: "Web Design",
        src: '../images/work/Layer 34.png'
    },
    {
        category: "Web Design",
        src: '../images/work/Layer 35.png'
    },
    {
        category: "Landing Pages",
        src: '../images/work/Layer 36.png'
    },
    {
        category: "Landing Pages",
        src: '../images/work/Layer 37.png'
    },
    {
        category: "Landing Pages",
        src: '../images/work/Layer 38.png'
    },
    {
        category: "Landing Pages",
        src: '../images/work/Layer 39.png'
    },
    {
        category: "Landing Pages",
        src: '../images/work/Layer 40.png'
    },
    {
        category: "Landing Pages",
        src: '../images/work/Layer 41.png'
    },
    {
        category: "Wordpress",
        src: '../images/work/Layer 42.png'
    },
    {
        category: "Wordpress",
        src: '../images/work/Layer 43.png'
    },
    {
        category: "Wordpress",
        src: '../images/work/Layer 44.png'
    },
    {
        category: "Wordpress",
        src: '../images/work/Layer 45.png'
    },
    {
        category: "Wordpress",
        src: '../images/work/Layer 46.png'
    },
    {
        category: "Wordpress",
        src: '../images/work/Layer 47.png'
    }
];

const workTabs = document.querySelectorAll('.work-tabs-title');
const workPhotos = document.querySelector('.work-photos');
const loadButton = document.querySelector('.load-button');

let loadedPhotos = [];
let visibleCount = 12;

const visiblePhotos = () => {
    workPhotos.innerHTML = '';
    loadedPhotos.forEach((photo, i) => {
        if (i < visibleCount) {
            workPhotos.insertAdjacentHTML(
                'beforeend',
                `
                <div class="photo-hover">
                    <img src="${photo.src}" class="work-photo-hover ${photo.category.split(' ').join('')}"></img>
                    <div class="work-content-hover">
                        <img src="images/work/photo-hover.svg" alt="link">
                        <div class="photo-text-hover">
                            <p class="photo-name">Creative Design</p>
                            <p class="photo-category">${photo.category}</p>
                        </div>
                    </div>
                </div>
                `
            );
        }
    });
};

const filterPhotos = (category) => {
    if (category === 'All') {
        loadedPhotos = photos;
    } else {
        loadedPhotos = photos.filter(photo => photo.category === category);
    }

    visiblePhotos();
};

workTabs.forEach(tab => {
    tab.addEventListener('click', (event) => {
        workTabs.forEach(t => t.classList.remove('work-background'));
        event.target.classList.add('work-background');
        filterPhotos(event.target.dataset.filter);
    });
});

loadButton.addEventListener('click', () => {
    visibleCount += 12;
    visiblePhotos();

    if (visibleCount >= loadedPhotos.length) {
        loadButton.style.display = 'none';
    }
});

filterPhotos('All');

//---------------------------What people say---------------------------------

let data = [
    {
        comment: "Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis.",
        name: "Hasan Ali",
        job: "UX Designer",
        avatar: "./images/what-people-say/avatar1.png"
    },
    {
        comment: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Autem, quae temporibus? Autem in aut a!",
        name: "Lara Meyers",
        job: "Project Manager",
        avatar: "./images/what-people-say/avatar2.png"
    },
    {
        comment: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Autem sapiente dolore incidunt iste corporis voluptatem modi pariatur similique eligendi accusantium. Id, quos sed.",
        name: "Dewi Brock",
        job: "Front-End Developer",
        avatar: "./images/what-people-say/avatar3.png"
    },
    {
        comment: "Lorem ipsum dolor sit amet, consectetur adipisicing.",
        name: "Kiara Proctor",
        job: "Business Analyst",
        avatar: "./images/what-people-say/avatar4.png"
    }
];

let currentIndex = 0;

function updateCarousel() {
    const comments = document.querySelectorAll('.people-say-comment');
    const names = document.querySelectorAll('.people-say-man-name');
    const jobs = document.querySelectorAll('.people-say-man-job');
    const avatars = document.querySelectorAll('.people-say-man-avatar-photo');

    comments.forEach((comment, index) => {
        comment.textContent = data[currentIndex].comment;
        names[index].textContent = data[currentIndex].name;
        jobs[index].textContent = data[currentIndex].job;
        avatars[index].src = data[currentIndex].avatar;
    });
}

let switcherAvatars = document.querySelectorAll('.switcher-avatar');

switcherAvatars.forEach((avatar, index) => {
    avatar.addEventListener('click', () => {
        currentIndex = index;
        updateCarousel();
        activeAvatar();
    });
});

function activeAvatar() {

    switcherAvatars.forEach((avatar) => {
        avatar.classList.remove('active');
    });

    switcherAvatars[currentIndex].classList.add('active');
}

const leftArrow = document.querySelector('.left-arrow');
const rightArrow = document.querySelector('.right-arrow');

leftArrow.addEventListener('click', () => {
    currentIndex = (currentIndex - 1 + data.length) % data.length;
    updateCarousel();
    activeAvatar();
});

rightArrow.addEventListener('click', () => {
    currentIndex = (currentIndex + 1) % data.length;
    updateCarousel();
    activeAvatar();
});
